"""Entry point for wavestream-sdk-python."""

from .cli import main  # pragma: no cover

if __name__ == "__main__":  # pragma: no cover
    main()
