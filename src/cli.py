import time
import wavestream_openapi_client
from pprint import pprint
from wavestream_openapi_client.apis.paths.ping import Ping
from wavestream_openapi_client.model.app import App
from wavestream_openapi_client.model.problem import Problem


def main():
    configuration = wavestream_openapi_client.Configuration(
        api_key='ce8a67581f7bec70c53de6848f6840197a8b807853bbb55caa8b233e9ae5e9636240fc723de58fe2c07668686f15cd10972b'
    )

    # Enter a context with an instance of the API client
    with wavestream_openapi_client.ApiClient(configuration) as api_client:
        # Create an instance of the API class
        api_instance = Ping.get(api_client)

        try:
            # Index of applications
            api_response = api_instance.get_apps()
            pprint(api_response)
        except wavestream_openapi_client.ApiException as e:
            print("Exception when calling AppApi->get_apps: %s\n" % e)